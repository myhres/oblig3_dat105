#ifndef COMPOSEMAIL_H
#define COMPOSEMAIL_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QTextEdit>
#include <QHBoxLayout>
#include <QVBoxLayout>

// Compose mail window
class ComposeMail : public QWidget {
    Q_OBJECT

    QVBoxLayout *mainVBox;
    QVBoxLayout *vbox1;
    QVBoxLayout *vbox2;
    QHBoxLayout *hbox1;
    QHBoxLayout *hbox2;

    QLabel *toLabel;
    QLabel *subjectLabel;

    QLineEdit *toLineEdit;
    QLineEdit *subjectLineEdit;

    QPushButton *sendBtn;
    QPushButton *cancelBtn;

    QTextEdit *textEdit;
public:
    explicit ComposeMail(QWidget *parent = 0);

signals:
public slots:
    void sendBtnClicked();
    void cancelBtnClicked();
};

#endif // COMPOSEMAIL_H
