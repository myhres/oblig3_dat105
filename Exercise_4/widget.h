#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QListView>
#include <QStringListModel>
#include <QStringList>

#include "composemail.h"

class Widget : public QWidget {
    Q_OBJECT

    QHBoxLayout *hbox;
    QVBoxLayout *vbox;

    QPushButton *checkBtn;
    QPushButton *deleteBtn;
    QPushButton *newBtn;

    QListView *listView;
    QStringListModel *stringListModel;
    QStringList *stringList;

    ComposeMail *composeWindow;
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

public slots:
    void checkBtnClicked();
    void deleteBtnClicked();
    void newBtnClicked();

private:
};

#endif // WIDGET_H
