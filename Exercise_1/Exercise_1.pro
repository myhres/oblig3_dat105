TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    vqueue.cpp \
    edge.cpp \
    priorityqueue.cpp

HEADERS += \
    vqueue.h \
    node.h \
    edge.h \
    priorityqueue.h
