#include "priorityqueue.h"

// Constructor
PriorityQueue::PriorityQueue() {
    this->sz = 0;
}

// Adds elements. Highest priority first, else FIFO
void PriorityQueue::add(Edge *e) {
    if(isEmpty()) {
        this->queue.push_back(e);
        this->sz++;
    }
    else {
        bool inserted = false;

        // finn rett plass
        for(int i = 0; i < this->sz; i++) {
            if(queue[i]->weight > e->weight) { // e har større prioritet
                this->shiftBack(i);
                queue[i] = e;
                this->sz++;

                inserted = true;
                return;
            }
        }
        if(!inserted) { // ingen med lavere prioritet
            queue.push_back(e);
            this->sz++;
        }
    }
}

// Removes element with highest priority
Edge* PriorityQueue::remove() {
    Edge *r = nullptr;

    if(!this->isEmpty()) {
        r = queue[0];
        this->shiftForward();
        this->sz--;
    }

    return r;
}

// Shifts elements back from a point
void PriorityQueue::shiftBack(int from) {
    queue.push_back(queue[sz-1]);

    for(int i = sz-2; i >= from; i--) {
        queue[i+1] = queue[i];
    }

}

// Shifts all elements forward
void PriorityQueue::shiftForward() {
    for(int i = 0; i < this->sz - 1; i++) {
        queue[i] = queue[i+1];
    }
}

// Checks if queue is empty
bool PriorityQueue::isEmpty() {
    return this->sz <= 0;
}

// Prints queue to console
void PriorityQueue::printQueue() {
    std::cout << "size: " << sz << " [";
    for(int i = 0; i < this->sz; i++) {
        ///std::cout << queue[i]->endpoint[0]->data << queue[i]->endpoint[1]->data << queue[i]->weight << " ";
        std::cout << queue[i] << " ";
    }
    std::cout << "]" << std::endl;
}

// Overloads += operator. Works as add()
PriorityQueue& PriorityQueue::operator +=(Edge& rhs) {
    this->add(&rhs);
    return *this;
}
