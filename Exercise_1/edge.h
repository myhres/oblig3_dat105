#ifndef EDGE_H
#define EDGE_H
#include <iostream>
#include "node.h"

class Edge {
public:
    Edge(Node* v, Node* w, int wt = 0, bool d = false);
    bool isDirected;
    int weight;
    Node* endpoint[2];
};
std::ostream& operator<<(std::ostream&, const Edge*);

#endif // EDGE_H
