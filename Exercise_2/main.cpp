#include <iostream>
#include "graph.h"

using namespace std;

std::ostream& operator<<(std::ostream& os, const Edge* e){
    os << "(" << e->endpoint[0]->data << ", " <<  e->endpoint[1]->data << ")";
    return os;
}

std::ostream& operator<<(std::ostream& os, const Node* n){
    char previous = ' ';
    if(n->previous != NULL)
            previous = n->previous->data;
    os << "(" << n->data << ", " <<  n->distance << ", " << previous << ")";
    return os;
}

template <typename T>
void printVector(const vector<T>& v){
    for(typename vector<T>::const_iterator i = v.begin(); i != v.end(); i++){
        cout << *i << endl;
    }
}

int main()
{
    Graph graf;
    Node* a = new Node('a');
    Node* b = new Node('b');
    Node* c = new Node('c');
    Node* d = new Node('d');
    Node* e = new Node('e');
    Node* f = new Node('f');

    graf.insertVertex(a);
    graf.insertVertex(b);
    graf.insertVertex(c);
    graf.insertVertex(d);
    graf.insertVertex(e);
    graf.insertVertex(f);

    graf.insertEdge(a,b,1);
    graf.insertEdge(b,c,2);
    graf.insertEdge(b,d,3);
    graf.insertEdge(c,e,4);
    graf.insertEdge(d,e,5);
    graf.insertEdge(e,f,1);

    /// BFT:
    cout << "Breadth first traversal:" << endl;
    graf.breadthFirstTraversal(f);

    cout << "Depth first traversal:" << endl;
    graf.depthFirstTraversal(f);

    /// PRIMS:
    cout << "Prim's algorithm:" << endl;
    vector<Edge*> v = graf.primsAlgorithm();
    printVector(v);

    cout << "Dijkstra's algorithm:" << endl;
    graf.dijkstrasAlgorithm(e);
    printVector(graf.vertices());

    cout << "Approx vertex cover:" << endl;
    printVector(graf.approxVertexCover());
    cout << graf.checkVertexCover(graf.approxVertexCover()) << endl;

    /// REMOVE VERTEX:
    cout << "Remove vertex:" << endl;
    cout << "Før sletting av 'b':" << endl;
    printVector(graf.vertices());
    printVector(graf.edges());
    cout << graf.numEdges() << " " << graf.numVertices() << endl;
    cout << "Etter sletting:" << endl;
    graf.removeVertex(b);
    printVector(graf.vertices());
    printVector(graf.edges());
    cout << graf.numEdges() << " " << graf.numVertices() << endl;

    /// REMOVE EDGE:
    cout << "Remove edge:" << endl;
    cout << "Før sletting av (e,f,1):" << endl;
    printVector(graf.edges());
    cout << "Etter sletting:" << endl;
    graf.removeEdge(new Edge(e,f,1));
    printVector(graf.edges());

    return 0;
}

